// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com
/** @file
* @brief Default configuration options that can be overridden in main config file
* kmTimer0DefaultConfig.h
*
*  **Created on**: May 03, 2022 @n
*      **Author**: Krzysztof Moskwa @n
*      **License**: GPL-3.0-or-later @n@n
*
*  kmDebug library for AVR MCUs @n
*  **Copyright (C) 2019  Krzysztof Moskwa**
*
*  This program is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 3 of the License, or
*  (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/


#ifndef KM_TIMER0_DEFAULT_CONFIG_H_
#define KM_TIMER0_DEFAULT_CONFIG_H_

/// Defines minimum possible steps for accurate time PWM timer functions. In case less value is calculated, this will be used
#define KM_TCC0_MINIMUM_PWM_CYCCLE_ACCURACY 4

#endif /* KM_TIMER0_DEFAULT_CONFIG_H_ */