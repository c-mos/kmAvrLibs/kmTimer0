/*
This is an independent project of an individual developer. Dear PVS-Studio, please check it.
PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com
*//** @file
* @mainpage
* @section pageTOC Content
* @brief The kmTimer0 library provides functions to initialize and control Timer 5 on AVR MCUs.
* - kmTimer0.h
* - kmTimer0DefaultConfig.h
*
*  **Created on**: May 03, 2022 @n
*      **Author**: Krzysztof Moskwa @n
*      **License**: GPL-3.0-or-later @n
*
*  **Copyright (C) 2022  Krzysztof Moskwa**
*  
*  This program is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 3 of the License, or
*  (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


#ifndef KM_TIMER_ZERO_H_
#define KM_TIMER_ZERO_H_

#ifdef KM_DOXYGEN
#ifdef OCR0
#undef OCR0
#endif /* OCR0 */
#endif /* KM_DOXYGEN */

#include <stdbool.h>

#include "../kmCommon/kmCommon.h"
#include "../kmTimersCommon/kmTimerDefs.h"

/// Remapping macro for saving own user data in callback registration #timer0RegisterCallbackCompA function
#define KM_TIMER0_USER_DATA(X) (void *)(X)

/**
Definition of the Timer0 Callback Type
@param Pointer for void user data content that is used in #timer0RegisterCallbackCompA function
and will be delivered to callback."as is"
*/
typedef void kmTimer0CallbackType(void *);

/**
Enumeration type definition of the PWM outputs for timer used in parameters of #kmTimer0SetPwmDuty, #kmTimer0SetPwmDuty, #kmTimer0SetPwmInversion functions .
*/
typedef enum {
	/// PWM on output A.
	  KM_TCC0_PWM_OUT_A
#ifndef OCR0
	,
	/// PWM on output B.
	KM_TCC0_PWM_OUT_B
#endif
} Tcc0PwmOut;

/**
Generic function to initialize Timer 0 with given prescaler and mode (see kmTimersCommon/kmTimer0Defs.h fore details)
@param prescaler selects one of prescaler as defined in kmTimersCommon/kmTimer0Defs.h (e.g. KM_TCC0_PRSC_256)
@param modeA Value definition of Timer/Counter register - part A (e.g. \b KM_TCC0_MODE_2_A for CTC mode)
@param modeB Value definition of Timer/Counter register - part B (e.g. \b KM_TCC0_MODE_2_B for CTC mode)
*/
void kmTimer0Init(const uint8_t prescaler, const uint8_t modeA, const uint8_t modeB);

/**
Initializes Timer 0 in the mode allowing to use CompA, CompB and Overflow Interrupts launched at specific phase of timer.
The timer goes always from 0 to KM_\b TIMER0_MAX value (255), callbacks for CompA and CompB are launched
once timer reaches specific values set by #timer0SetValueCompA and #timer0SetValueCompB
The callbacks needs to be configured with #kmTimer0RegisterCallbackOVF, #kmTimer0RegisterCallbackCompA and #kmTimer0RegisterCallbackCompB functions.
The period/frequency of each of these Interrupts will be exactly the same and defined by MCU main clock and the prescaler.
Overflow callback is called when Timer 0 reaches value \b KM_TIMER0_MAX (255)
CompA and CompB callbacks are called when Timer 0 reaches values set by #timer0SetValueCompA and #timer0SetValueCompB respectively.
Setting these values allows to shift callback calls with phase (with accuracy limited with time that used by MCU to execute code within callback)
@param prescaler selects one of prescaler as defined in kmTimersCommon/kmTimer0Defs.h (e.g. \b KM_TCC0_PRSC_256)

@code
#define KM_TIMER0_TEST_USER_DATA_A 1UL
#define KM_TIMER0_TEST_USER_DATA_B 65535UL
#define KM_TIMER0_TEST_USER_DATA_C 255UL
#define KM_TIMER0_TEST_DUTY_0_PERC KM_TIMER0_BOTTOM
#define KM_TIMER0_TEST_DUTY_25_PERC KM_TIMER0_MID - (KM_TIMER0_MID >> 1)
#define KM_TIMER0_TEST_DUTY_50_PERC KM_TIMER0_MID
#define KM_TIMER0_TEST_DUTY_75_PERC KM_TIMER0_MID + (KM_TIMER0_MID >> 1)
#define KM_TIMER0_TEST_DUTY_100_PERC KM_TIMER0_TOP

#include "kmCpu/kmCpu.h"
#include "kmDebug/kmDebug.h"
#include "kmTimersCommon/kmTimerDefs.h"
#include "kmTimer0/kmTimer0.h"

void callbackOVF(void *userData) {
	dbToggle(DB_PIN_0);
	dbOn(DB_PIN_1);
	dbOn(DB_PIN_2);
}

void callbackCompAOff(void *userData) {
	dbOff(DB_PIN_1);
}

void callbackCompBOff(void *userData) {
	dbOff(DB_PIN_2);
}

int main(void) {
	appInitDebug();
	kmCpuInterruptsEnable();

	kmTimer0InitOnPrescalerBottomToTopOvfCompABInterruptCallback(KM_TCC0_PRSC_8);

	kmTimer0RegisterCallbackOVF(KM_TIMER0_USER_DATA(KM_TIMER0_TEST_USER_DATA_B), callbackOVF);
	kmTimer0EnableInterruptOVF();

	kmTimer0RegisterCallbackCompA(KM_TIMER0_USER_DATA(KM_TIMER0_TEST_USER_DATA_A), callbackCompAOff);
	kmTimer0SetValueCompA(KM_TIMER0_TEST_DUTY_25_PERC);
	kmTimer0EnableInterruptCompA();

	kmTimer0RegisterCallbackCompB(KM_TIMER0_USER_DATA(KM_TIMER0_TEST_USER_DATA_C), callbackCompBOff);
	kmTimer0SetValueCompB(KM_TIMER0_TEST_DUTY_75_PERC);
	kmTimer0EnableInterruptCompB();

	kmTimer0Start();

	while(true) {
	}
@endcode
Output from the example code:
\image html kmTimer0Test2.png
*/
void kmTimer0InitOnPrescalerBottomToTopOvfCompABInterruptCallback(const uint8_t prescaler);

/**
Initializes Timer 0 in the mode allowing to generate frequency with specific period on OCA0 output.
This function tries to configure Timer0 to make the period as accurate as possible - 
see limitations in the description of #kmTimer0CalcPerdiod function.
To change period while Timer0 is running calculate new CompA value and use #kmTimer0SetValueCompA and #kmTime0SetPrescale functions.
with kmTimer0CalcPerdiod shifting new periodInMicroseconds by one bit to the right
@param periodInMicroseconds period of the square function on the OCA0, the available range depends on the main MCU clock

Example code:
@code
	kmTimer0InitOnAccuratePeriodGenerateOutputClockA(KM_TIMER0_TEST_1MS);
	kmTimer0Start();
@endcode
Output from the example code:
\image html kmTimer0Test0.png
*/
void kmTimer0InitOnAccuratePeriodGenerateOutputClockA(const uint32_t periodInMicroseconds);

/**
Initializes Timer 0 in the mode allowing to use CompA Interrupt launched every specific period defined in microseconds.
The callback needs to be configured with #kmTimer0RegisterCallbackCompA function.
It's also possible to generate the square wave toggling output with the same period (output frequency is half of frequency of executing callbacks)
Note accuracy is limited with time that used by MCU to execute code within callback.
@param microseconds time period between callback calls defined in microseconds, the available range depends on the main MCU clock

@code
	
(KM_TIMER0_TEST_5MS, true);

	kmTimer0RegisterCallbackCompA(KM_TIMER0_USER_DATA(KM_TIMER0_TEST_USER_DATA_A), callbackCompAToggle);
	kmTimer0EnableInterruptCompA();

	kmTimer0Start();
}
@endcode
Output from the example code:
\image html kmTimer0Test1.png
*/
void kmTimer0InitOnAccurateTimeCompAInterruptCallback(const uint32_t microseconds, const bool outOnOC0A);

/**
Initializes Timer 0 as a Counter of either rising or falling signal edges on the input T0
@param falling if true the counter will count falling edges, if false - rising edges
*/
void kmTimer0InitExternal(bool falling);

#ifndef OCR0
/**
Initializes Timer 0 in the mode allowing to use CompA and CompB Interrupt launched every specific period defined in microseconds.
The CompB Interrupt can be shifted in phase by providing corresponding phaseIntB parameter
The callbacks needs to be configured with #kmTimer0RegisterCallbackCompA and #kmTimer0RegisterCallbackCompB functions.
The timer goes always from 0 to top value defined as calculation for microseconds used as parameter.
This function tries to define period of calling the callbacks as accurate as possible
The range of available frequencies depends on used XTAL\n
\b NOTE: This initialization function is not available on the older models of AVR MCUs (like ATmega8 or ATmega64)

@param microseconds time period between callback calls defined in microseconds, the available range depends on the main MCU clock
@param phaseIntB phase shift between executing CompA and CompB callbacks. The range of the value from 0 to 255, where 0 means no phase shift and 128 means 180 phase shift.
The accuracy of the phase shift depends on on the main MCU clock and the microseconds parameter

@code
	kmTimer0InitOnAccurateTimeCompABInterruptCallback(KM_TIMER0_TEST_1MS, KM_TIMER0_TEST_PHASE_180_DEG); // for 1 millisecond

	kmTimer0RegisterCallbackCompA(KM_TIMER0_USER_DATA(KM_TIMER0_TEST_USER_DATA_C), callbackCompAToggle);
	kmTimer0EnableInterruptCompA();

	kmTimer0RegisterCallbackCompB(KM_TIMER0_USER_DATA(KM_TIMER0_TEST_USER_DATA_A), callbackCompBToggle);
	kmTimer0EnableInterruptCompB();
	kmTimer0Start();
@endcode
Output from the example code:
\image html kmTimer0Test5.png
*/
void kmTimer0InitOnAccurateTimeCompABInterruptCallback(const uint32_t microseconds, uint8_t phaseIntB);

/**
Initializes Timer 0 to generate square wave on the OC0B pin of the MCU with specified time period and duty cycle (Fast PWM mode)
To change the duty cycle after initialization #kmTimer0SetPwmDuty function can be used 
(only #KM_TCC0_PWM_OUT_B can be used as pwmOut parameter, executing this function KM_TCC0_PWM_OUT_A may cause unpredictable results)\n
OVF, CompA and CompB callback functions can be used with this mode to capture desired moments within Timer 0 periods\n
\b NOTE: This initialization function is not available on the older models of AVR MCUs (like ATmega8 or ATmega64)

@param microseconds time period of the square wave generated on OC0B pin of MCU, the available range and accuracy depends on the main MCU clock
@param outputCompA if true the square wave corresponding to half frequency of period defined in 
\e microseconds parameter is generated on the OC0A pin of MCU (without possibility to define duty)
@param dutyB duty of the square wave generated on the OC0B pin of the MCU; the range from 0 to 255, 
where 64 means 25% and 128 means 50% duty cycle; the accuracy of the value depends on the main clock of the MCU and \e microseconds parameter, 
the function calculates it in the way to be at least equal or higher than #KM_TCC0_MINIMUM_PWM_CYCCLE_ACCURACY defined in kmTimer0DefaultConfig
In case this cannot be achieved, then the output frequency is decreased to keep the duty resolution defined in #KM_TCC0_MINIMUM_PWM_CYCCLE_ACCURACY\n
\b NOTE: The output on OC0B is activated within this initialization only when this parameter is greater than 0
@param invertedB if true, the output of the square wave generate don OC0B pin is inverted

@code
	uint8_t cyclesRange = kmTimer0InitOnAccurateTimeFastPwm(KM_TIMER0_TEST_1MS, true, KM_TIMER0_TEST_DUTY_25_PERC, false);

	kmTimer0RegisterCallbackOVF(KM_TIMER0_USER_DATA(KM_TIMER0_TEST_USER_DATA_B), callbackOVFToggle);
	kmTimer0EnableInterruptOVF();

	kmTimer0RegisterCallbackCompA(KM_TIMER0_USER_DATA(KM_TIMER0_TEST_USER_DATA_C), callbackCompAToggle);
	kmTimer0EnableInterruptCompA();

	kmTimer0RegisterCallbackCompB(KM_TIMER0_USER_DATA(KM_TIMER0_TEST_USER_DATA_C), callbackCompBToggle);
	kmTimer0EnableInterruptCompB();

	kmTimer0Start();

	// to change duty on the channel B
	testSwipePwm(KM_TCC0_PWM_OUT_B, cyclesRange);

#endif	
@endcode
Output from the example code:
\image html kmTimer0Test5.png
*/

uint8_t kmTimer0InitOnAccurateTimeFastPwm(const uint32_t microseconds, const bool outputCompA, const uint8_t dutyB, const bool invertedB);

/**
Initializes Timer 0 to generate square wave on the OC0B pin of the MCU with specified time period and duty cycle (Phase Correct PWM mode)
To change the duty cycle after initialization #kmTimer0SetPwmDuty function can be used
(only #KM_TCC0_PWM_OUT_B can be used as pwmOut parameter, executing this function KM_TCC0_PWM_OUT_A may cause unpredictable results)\n
OVF, CompA and CompB callback functions can be used with this mode to capture desired moments within Timer 0 periods\n
\b NOTE: This initialization function is not available on the older models of AVR MCUs (like ATmega8 or ATmega64)

@param microseconds time period of the square wave generated on OC0B pin of MCU, the available range and accuracy depends on the main MCU clock
@param outputCompA if true the square wave corresponding to half frequency of period defined in
\e microseconds parameter is generated on the OC0A pin of MCU (without possibility to define duty)
@param dutyB duty of the square wave generated on the OC0B pin of the MCU; the range from 0 to 255,
where 64 means 25% and 128 means 50% duty cycle; the accuracy of the value depends on the main clock of the MCU and \e microseconds parameter,
the function calculates it in the way to be at least equal or higher than #KM_TCC0_MINIMUM_PWM_CYCCLE_ACCURACY defined in kmTimer0DefaultConfig
In case this cannot be achieved, then the output frequency is decreased to keep the duty resolution defined in #KM_TCC0_MINIMUM_PWM_CYCCLE_ACCURACY\n
\b NOTE: The output on OC0B is activated within this initialization only when this parameter is greater than 0
@param invertedB if true, the output of the square wave generate don OC0B pin is inverted

@code
	uint8_t cyclesRange = kmTimer0InitOnAccurateTimePcPwm(KM_TIMER0_TEST_1MS, true, KM_TIMER0_TEST_DUTY_25_PERC, false);
	kmTimer0Start();

	kmTimer0RegisterCallbackOVF(KM_TIMER0_USER_DATA(KM_TIMER0_TEST_USER_DATA_B), callbackOVFToggle);
	kmTimer0EnableInterruptOVF();

	kmTimer0RegisterCallbackCompA(KM_TIMER0_USER_DATA(KM_TIMER0_TEST_USER_DATA_C), callbackCompAToggle);
	kmTimer0EnableInterruptCompA();

	kmTimer0RegisterCallbackCompB(KM_TIMER0_USER_DATA(KM_TIMER0_TEST_USER_DATA_C), callbackCompBToggle);
	kmTimer0EnableInterruptCompB();

	testSwipePwm(KM_TCC0_PWM_OUT_B, KM_TIMER0_MAX);
@endcode
Output from the example code:
\image html kmTimer0Test7.png
*/
uint8_t kmTimer0InitOnAccurateTimePcPwm(const uint32_t microseconds, const bool outputCompA, const uint8_t dutyB, const bool invertedB);

/**
Initializes Timer 0 to generate square wave on the OC0x pin of the MCU with specified duty cycle and frequency defined by prescaler only (Fast PWM mode)
To change the duty cycle after initialization #kmTimer0SetPwmDuty function can be used (for both #KM_TCC0_PWM_OUT_A and #KM_TCC0_PWM_OUT_B)
OVF, CompA and CompB callback functions can be used with this mode to capture desired moments within Timer 0 periods\n
\b NOTE: This initialization function is not available on the older models of AVR MCUs (like ATmega8 or ATmega64)

@param prescaler selects one of prescaler as defined in kmTimersCommon/kmTimer0Defs.h (e.g. KM_TCC0_PRSC_256)
@param dutyA duty of the square wave generated on the OC0A pin of the MCU; the range from 0 to 255,
where 64 means 25% and 128 means 50% duty cycle; this initialization offers maximum accuracy of the duty cycle,
\b NOTE: The output on OC0A is activated within this initialization only when this parameter is greater than 0
@param invertedA if true, the output of the square wave generate don OC0A pin is inverted
@param dutyB duty of the square wave generated on the OC0B pin of the MCU; the range from 0 to 255, 
where 64 means 25% and 128 means 50% duty cycle; this initialization offers maximum accuracy of the duty cycle, 
\b NOTE: The output on OC0B is activated within this initialization only when this parameter is greater than 0
@param invertedB if true, the output of the square wave generate don OC0B pin is inverted

@code
kmTimer0InitOnPrescalerBottomToTopFastPwm(KM_TCC0_PRSC_64, KM_TIMER0_TEST_DUTY_25_PERC, false, KM_TIMER0_TEST_DUTY_75_PERC, false);

kmTimer0RegisterCallbackOVF(KM_TIMER0_USER_DATA(KM_TIMER0_TEST_USER_DATA_C), callbackOVFToggle);
kmTimer0EnableInterruptOVF();

kmTimer0RegisterCallbackCompA(KM_TIMER0_USER_DATA(KM_TIMER0_TEST_USER_DATA_A), callbackCompAToggle);
kmTimer0EnableInterruptCompA();

kmTimer0RegisterCallbackCompB(KM_TIMER0_USER_DATA(KM_TIMER0_TEST_USER_DATA_B), callbackCompBToggle);
kmTimer0EnableInterruptCompB();

kmTimer0Start();

testSwipePwm(KM_TCC0_PWM_OUT_A, KM_TIMER0_MAX);
testSwipePwm(KM_TCC0_PWM_OUT_B, KM_TIMER0_MAX);
@endcode
Output from the example code:
\image html kmTimer0Test4.png
*/
void kmTimer0InitOnPrescalerBottomToTopFastPwm(const uint8_t prescaler, const uint8_t dutyA, const bool invertedA, const uint8_t dutyB, const bool invertedB);

/**
Initializes Timer 0 to generate square wave on the OC0x pin of the MCU with specified duty cycle and frequency defined by prescaler only (Phase Correct PWM mode)
To change the duty cycle after initialization #kmTimer0SetPwmDuty function can be used (for both #KM_TCC0_PWM_OUT_A and #KM_TCC0_PWM_OUT_B)
OVF, CompA and CompB callback functions can be used with this mode to capture desired moments within Timer 0 periods\n
\b NOTE: This initialization function is not available on the older models of AVR MCUs (like ATmega8 or ATmega64)

@param prescaler selects one of prescaler as defined in kmTimersCommon/kmTimer0Defs.h (e.g. KM_TCC0_PRSC_256)
@param dutyA duty of the square wave generated on the OC0A pin of the MCU; the range from 0 to 255,
where 64 means 25% and 128 means 50% duty cycle; this initialization offers maximum accuracy of the duty cycle,
\b NOTE: The output on OC0A is activated within this initialization only when this parameter is greater than 0
@param invertedA if true, the output of the square wave generate don OC0A pin is inverted
@param dutyB duty of the square wave generated on the OC0B pin of the MCU; the range from 0 to 255,
where 64 means 25% and 128 means 50% duty cycle; this initialization offers maximum accuracy of the duty cycle,
\b NOTE: The output on OC0B is activated within this initialization only when this parameter is greater than 0
@param invertedB if true, the output of the square wave generate don OC0B pin is inverted

@code
kmTimer0InitOnPrescalerBottomToTopPcPwm(KM_TCC0_PRSC_64, KM_TIMER0_TEST_DUTY_25_PERC, false, KM_TIMER0_TEST_DUTY_75_PERC, false);

kmTimer0RegisterCallbackOVF(KM_TIMER0_USER_DATA(KM_TIMER0_TEST_USER_DATA_C), callbackOVFToggle);
kmTimer0EnableInterruptOVF();

kmTimer0RegisterCallbackCompA(KM_TIMER0_USER_DATA(KM_TIMER0_TEST_USER_DATA_A), callbackCompAToggle);
kmTimer0EnableInterruptCompA();

kmTimer0RegisterCallbackCompB(KM_TIMER0_USER_DATA(KM_TIMER0_TEST_USER_DATA_B), callbackCompBToggle);
kmTimer0EnableInterruptCompB();

kmTimer0Start();

testSwipePwm(KM_TCC0_PWM_OUT_A, KM_TIMER0_MAX);
testSwipePwm(KM_TCC0_PWM_OUT_B, KM_TIMER0_MAX);
@endcode
Output from the example code:
\image html kmTimer0Test6.png
*/
void kmTimer0InitOnPrescalerBottomToTopPcPwm(const uint8_t prescaler, const uint8_t dutyA, const bool invertedA, const uint8_t dutyB, const bool invertedB);
#endif

/**
\deprecated Can be used with older models of AVR MCUs like ATmega8, ATmega32, etc.\n
Initializes Timer 0 to generate square wave on the OC0A pin of the MCU with specified duty cycle and frequency defined by prescaler only (Fast PWM mode)
To change the duty cycle after initialization #kmTimer0SetPwmDuty function can be used (#KM_TCC0_PWM_OUT_A can only be used)
OVF and CompA callback functions can be used with this mode to capture desired moments within Timer 0 periods\n

@param prescaler selects one of prescaler as defined in kmTimersCommon/kmTimer0Defs.h (e.g. KM_TCC0_PRSC_256)
@param dutyA duty of the square wave generated on the OC0A pin of the MCU; the range from 0 to 255,
where 64 means 25% and 128 means 50% duty cycle; this initialization offers maximum accuracy of the duty cycle,
\b NOTE: The output on OC0A is activated within this initialization only when this parameter is greater than 0
@param invertedA if true, the output of the square wave generate don OC0A pin is inverted
*/
void kmTimer0InitOnPrescalerBottomToTopFastPwmOCA(const uint8_t prescaler, const uint8_t dutyA, const bool invertedA);

/**
\deprecated Can be used with older models of AVR MCUs like ATmega8, ATmega32, etc.\n
InitializesTimer 0 to generate square wave on the OC0A pin of the MCU with specified duty cycle and frequency defined by prescaler only (Phase Accurate mode)
To change the duty cycle after initialization #kmTimer0SetPwmDuty function can be used (#KM_TCC0_PWM_OUT_A can only be used)
OVF and CompA callback functions can be used with this mode to capture desired moments within Timer 0 periods\n

@param prescaler selects one of prescaler as defined in kmTimersCommon/kmTimer0Defs.h (e.g. KM_TCC0_PRSC_256)
@param dutyA duty of the square wave generated on the OC0A pin of the MCU; the range from 0 to 255,
where 64 means 25% and 128 means 50% duty cycle; this initialization offers maximum accuracy of the duty cycle,
\b NOTE: The output on OC0A is activated within this initialization only when this parameter is greater than 0
@param invertedA if true, the output of the square wave generate don OC0A pin is inverted
*/
void kmTimer0InitOnPrescalerBottomToTopPcPwmOCA(const uint8_t prescaler, const uint8_t dutyA, const bool invertedA);

// Setters callback user data
/**
Sets or changes User Data passed to the callback registered with #kmTimer0RegisterCallbackOVF.
The User Data is stored in (void *) type (uint16_t), so maximum 16 bit single value can be stored, or pointer to the function or structure
It's recommended to use #KM_TIMER0_USER_DATA macro to cast the data to correct format.\n
@param userData User Data as described above, example:\n

@code
	kmTimer0SetCallbackUserDataOVF(KM_TIMER0_USER_DATA(KM_TIMER0_TEST_USER_DATA_C));
@endcode
*/
void kmTimer0SetCallbackUserDataOVF(void *userData);

/**
Sets or changes User Data passed to the callback registered with #kmTimer0RegisterCallbackCompA.
The User Data is stored in (void *) type (uint16_t), so maximum 16 bit single value can be stored, or pointer to the function or structure
It's recommended to use #KM_TIMER0_USER_DATA macro to cast the data to correct format.\n
@param userData User Data as described above, example:\n

@code
	kmTimer0RegisterCallbackCompA(KM_TIMER0_USER_DATA(KM_TIMER0_TEST_USER_DATA_A));
@endcode
*/
void kmTimer0SetCallbackUserDataCompA(void *userData);

#ifndef OCR0
/**
Sets or changes User Data passed to the callback registered with #kmTimer0RegisterCallbackCompB.
The User Data is stored in (void *) type (uint16_t), so maximum 16 bit single value can be stored, or pointer to the function or structure
It's recommended to use #KM_TIMER0_USER_DATA macro to cast the data to correct format.\n
@param userData User Data as described above, example:\n

@code
	kmTimer0RegisterCallbackCompA(KM_TIMER0_USER_DATA(KM_TIMER0_TEST_USER_DATA_B));
@endcode
*/
void kmTimer0SetCallbackUserDataCompB(void *userData);
#endif /* OCR0 */

// Registering callbacks
/**
Functions to register callback launched on Timer0 Overflow. The callback needs to be declared with #kmTimer0CallbackType
The User Data is stored in (void *) type (uint16_t), so maximum 16 bit single value can be stored, or pointer to the function or structure
It's recommended to use #KM_TIMER0_USER_DATA macro to cast the data to correct format.\n
\b NOTE: The callback is unbuffered and is executed straight from the interrupt, so the routine should be as short as possible
@param userData User Data as described above
@param callback Callback declared with #kmTimer0CallbackType type

@code
void callbackOVFToggle(void *userData) {
	uint16_t a = (int16_t)userData; // example unwraping user data
	dbToggle(DB_PIN_0);
}

/// application routine
kmTimer0RegisterCallbackOVF(KM_TIMER0_USER_DATA(KM_TIMER0_TEST_USER_DATA_B), callbackOVFToggle);
@endcode
*/
void kmTimer0RegisterCallbackOVF(void *userData, kmTimer0CallbackType *callback);

/**
Functions to register callback launched on Timer0 Overflow Interrupt. The callback needs to be declared with #kmTimer0CallbackType
The User Data is stored in (void *) type (uint16_t), so maximum 16 bit single value can be stored, or pointer to the function or structure
It's recommended to use #KM_TIMER0_USER_DATA macro to cast the data to correct format.\n
\b NOTE: The callback is unbuffered and is executed straight from the interrupt, so the routine should be as short as possible
@param userData User Data as described above
@param callback Callback declared with #kmTimer0CallbackType type

@code
void callbackCompAToggle(void *userData) {
	uint16_t a = (int16_t)userData; // example unwraping user data
	dbToggle(DB_PIN_1);
}

/// application routine
kmTimer0RegisterCallbackCompA(KM_TIMER0_USER_DATA(KM_TIMER0_TEST_USER_DATA_A), callbackCompAToggle);
@endcode
*/
void kmTimer0RegisterCallbackCompA(void *userData, kmTimer0CallbackType *callback);

/**
Functions to register callback launched on Timer0 Overflow Interrupt. The callback needs to be declared with #kmTimer0CallbackType
The User Data is stored in (void *) type (uint16_t), so maximum 16 bit single value can be stored, or pointer to the function or structure
It's recommended to use #KM_TIMER0_USER_DATA macro to cast the data to correct format.\n
\b NOTE: The callback is unbuffered and is executed straight from the interrupt, so the routine should be as short as possible
@param userData User Data as described above
@param callback Callback declared with #kmTimer0CallbackType type

@code
void callbackCompBToggle(void *userData) {
	uint16_t a = (int16_t)userData; // example unwraping user data
	dbToggle(DB_PIN_1);
}

/// application routine
kmTimer0RegisterCallbackCompB(KM_TIMER0_USER_DATA(KM_TIMER0_TEST_USER_DATA_B), callbackCompBToggle);
@endcode
*/
void kmTimer0RegisterCallbackCompB(void *userData, kmTimer0CallbackType *callback);

// Unregistering callbacks
/**
Permanently unregisters callback registered with #kmTimer0RegisterCallbackOVF function
*/
void kmTimer0UnregisterCallbackOVF(void);

/**
Permanently unregisters callback registered with #kmTimer0RegisterCallbackCompA function
*/
void kmTimer0UnregisterCallbackCompA(void);

/**
Permanently unregisters callback registered with #kmTimer0RegisterCallbackCompB function.
*/
void kmTimer0UnregisterCallbackCompB(void);

// Enabling interrupts
/**
Enables Timer0 Overflow interrupt allowing to execute routine registered with #kmTimer0RegisterCallbackOVF
*/
void kmTimer0EnableInterruptOVF(void);

/**
Enables Timer0 Comparator A interrupt allowing to execute routine registered with #kmTimer0RegisterCallbackCompA
*/
void kmTimer0EnableInterruptCompA(void);

#ifndef OCR0
/**
Enables Timer0 Comparator B interrupt allowing to execute routine registered with #kmTimer0RegisterCallbackCompB
*/
void kmTimer0EnableInterruptCompB(void);
#endif /* OCR0 */


// Disabling interrupts
/**
Disables all Timer 0 interrupts (Overflow, ComparatorA & ComparatorB).
*/
void kmTimer0DisableInterruptsAll(void);

/**
Disables Timer 0 Overflow interrupt
*/
void kmTimer0DisableInterruptOVF(void);

/**
Disables Timer 0 ComparatorA interrupt
*/
void kmTimer0DisableInterruptCompA(void);

/**
Disables Timer 0 ComparatorB interrupt
*/
void kmTimer0DisableInterruptCompB(void);

// Setters for Comp values
/**
Sets value of Timer/Counter0 Output Compare Register A\n
\b NOTE: Although the possible values are from 0 to \b KM_TIMER0_MAX (0xFF), for calculations these should be considered as 1 to 256, so value 0 divides input frequency by 1, value 1 - divides by 2. etc
@param value of Timer/Counter0 Output Compare Register A (from 0 to \b KM_TIMER0_MAX)
*/
void kmTimer0SetValueCompA(uint8_t value);

#ifndef OCR0
/**
Sets value of Timer/Counter0 Output Compare Register B\n
\b NOTE: Although the possible values are from 0 to \b KM_TIMER0_MAX (0xFF), for calculations these should be considered as 1 to 256, so value 0 divides input frequency by 1, value 1 - divides by 2. etc 
@param value of Timer/Counter0 Output Compare Register B (from 0 to \b KM_TIMER0_MAX)
*/
void kmTimer0SetValueCompB(uint8_t value);
#endif

// Configuration of Comparator Outputs
/**
Configures Compare Output Mode channel A for Timer0 (\b OC0A output) and sets the pin assigned to this function as an output
@param compareOutputMode selects one of Compare Output Modes as defined in 
kmTimersCommon/kmTimer0Defs.h (e.g. \b KM_TCC0_A_COMP_OUT_TOGGLE)
*/
void kmTimer0ConfigureOCA(uint8_t compareOutputMode);

#ifndef OCR0
/**
Configures Compare Output Mode channel B for Timer0 (\b OC0B output - if available) and sets the pin assigned to this function as an output
@param compareOutputMode selects one of Compare Output Modes as defined in 
kmTimersCommon/kmTimer0Defs.h (e.g. \b KM_TCC0_B_PWM_COMP_OUT_CLEAR_UP_SET_DOWN)
*/
void kmTimer0ConfigureOCB(uint8_t compareOutputMode);
#endif

// Controlling timer flow
/**
Enables running of Timer 0, allowing to execute Timer/Counter function defined with Initialization functions
*/
void kmTimer0Start(void);

/**
Stops running of Timer 0
*/
void kmTimer0Stop(void);

/**
Restarts internal counter of Timer 0.
*/
void kmTimer0Restart(void);

// Controlling PWM

/**
Sets the Duty in the PWM modes initialized as Bottom-To-Top. Can be applied to both OC0A and OC0B outputs
@param pwmOut either KM_TCC0_PWM_OUT_A for \b OC0A or KM_TCC0_PWM_OUT_B for \b OC0B
@param duty duty of the square wave generated on the OC0A/OC0B pin of the MCU; the range from 0 to 255,
where 64 means 25% and 128 means 50% duty cycle; this initialization offers maximum accuracy of the duty cycle
*/
void kmTimer0SetPwmDutyBottomToTop(Tcc0PwmOut pwmOut, uint8_t duty);

/**
Sets the Duty in the PWM modes initialized as Accurate-Time. Can be applied to OC0A output only (as Comparator A is used to control general time period)
@param duty duty of the square wave generated on the OC0A/OC0B pin of the MCU; the range from 0 to 255,
where 64 means 25%/75% and 128 means 50%/50% duty cycle; this initialization offers maximum accuracy of the duty cycle
The accuracy of the value depends on the main clock of the MCU and \e microseconds parameter provided in initialization function
The function calculates it in the way to be at least equal or higher than #KM_TCC0_MINIMUM_PWM_CYCCLE_ACCURACY defined in kmTimer0DefaultConfig
*/
void kmTimer0SetPwmDutyAccurateTimeModes(uint8_t duty);

/**
Function allows to invert PWM output either on \b OC0A or on \b OC0B
@param pwmOut either KM_TCC0_PWM_OUT_A for \b OC0A or KM_TCC0_PWM_OUT_B for \b OC0B
@param inverted value true inverts the PWM output, normal operation when false
*/
void kmTimer0SetPwmInversion(Tcc0PwmOut pwmOut, bool inverted);

// Timer flow calculations
/**
Function calculates prescaler and comparator cycles for Timer on basis of desired comparator match period\n
| F_CPU		| Min Frequency [Hz] | Max period [us] |
| ----:		| ----:		| ----:		  |
| 32 768	|	0.063	|	8000000	  |
| 1 000 000	|	1.907	|	262 144	  |
| 1 843 200	|	3.516	|	142 222	  |
| 2 000 000	|	3.815	|	131 072	  |
| 3 686 400	|	7.031	|	71 111	  |
| 4 000 000	|	7.629	|	65 536	  |
| 7 372 800	|	14.063	|	35 556	  |
| 8 000 000	|	15.259	|	32 768	  |
| 9 000 000	|	17.166	|	29 127	  |
| 11 059 200|	21.094	|	23 704	  |
| 14 745 600|	28.125	|	17 778	  |
| 16 000 000|	30.518	|	16 384	  |
| 18 432 000|	35.156	|	14 222	  |
| 20 000 000|	38.147	|	13 107	  |
| 24 000 000|	45.776	|	10 923	  |
| 25 000 000|	47.684	|	10 486	  |
Table above shows relation between main CPU clock and minimum frequencies available for Timer0

@param microseconds time period of the expected Comparator matches for Timer 0 
the available range and accuracy depends on the main MCU clock as specified in the table below
\b NOTE: Some modes of Timer0 require to divide number of microseconds by 2, this happens for example for 
Phase Correct modes, as Timer/Counter counts first up than down to get the proper result on the output.\n
@param prescaler result prescaler value as defined in \b kmTimersCommon\kmTimer0Defs.h to be used in #kmTimer0Init
@return comparator match value to be used in Timer initialization #kmTimer0Init function 
*/
uint8_t kmTimer0CalcPerdiod(const uint32_t microseconds, uint8_t *prescaler);

/**
Function calculates prescaler and comparator cycles for Timer on basis of desired comparator match period. This version takes into consideration minim resolution of PWM duty and decreases frequency if needed\n
@param microseconds time period of the expected Comparator matches for Timer 0
the available range and accuracy depends on the main MCU clock as specified in the table below.
Note this version of the function takes into consideration desired minimum resolution of the output square wave duty.
In practice this may only matter for very low main clock frequencies. If the desired minimum resolution cannot be reached,
this function will return prescaler and comparator match value decreasing desired period defined in milliseconds
\b NOTE: Some modes of Timer0 require to divide number of microseconds by 2, this happens for example for
Phase Correct modes, as Timer/Counter counts first up than down to get the proper result on the output.
@param prescaler result prescaler value as defined in \b kmTimersCommon\kmTimer0Defs.h to be used in #kmTimer0Init
@param minimumCycleAcuracy desired minimum resolution of the output square wave duty (e.g. 4 means that at least 0%, 25%, 50% and 75% are achievable)
@return comparator match value to be used in Timer initialization #kmTimer0Init function
*/
uint8_t kmTimer0CalcPerdiodWithMinPwmAccuracy(const uint32_t microseconds, uint8_t *prescaler, const uint8_t minimumCycleAcuracy);

/**
Calculates compare match result based on desired square wave duty and available compare match cycles
calculated for Accurate-Time modes (e.g. got as result of #kmTimer0CalcPerdiod or #kmTimer0InitOnAccurateTimeFastPwm)
@param duty desired duty cycle
@param cyclesRange available range of cycles for duty cycle
@return comparator match value corresponding to desired duty cycle (can be used e.g. in #kmTimer0SetValueCompA)
*/
uint8_t kmTimer0CalcDutyOnCycles(uint8_t duty, uint8_t cyclesRange);

#endif /* KM_TIMER_ZERO_H_ */
