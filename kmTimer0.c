/*
This is an independent project of an individual developer. Dear PVS-Studio, please check it.
PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com
*//*
* kmTimer0.c
*
*  **Created on**: May 03, 2022 @n
*      Author: Krzysztof Moskwa
*      License: GPL-3.0-or-later
*
*  kmDebug library for AVR MCUs
*  Copyright (C) 2019  Krzysztof Moskwa
*
*  This program is free software: you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation, either version 3 of the License, or
*  (at your option) any later version.
*
*  This program is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with this program.  If not, see <https://www.gnu.org/licenses/>.
*
*/

#include <stdlib.h>
#include <avr/interrupt.h>
#include <util/atomic.h>

#include "kmTimer0.h"

// Definitions
#define KM_TCC0_TOP KM_TCC_TOP_1

#define KM_TCC0_CYCLES_CALCULATION_CORRECTION 1u

// "Private" variables
static uint8_t _kmTimer0PrescalerSelectBits = 0;

static uint8_t _kmTimer0CompACycles = KM_TCC0_TOP;
static bool _kmTimer0OtputComparePinUsedA = false;

#ifndef OCR0
static uint8_t _kmTimer0CompBCycles = KM_TCC0_TOP;
static bool _kmTimer0OtputComparePinUsedB = false;
#endif

static kmTimer0CallbackType *_timerCallbackOVF = NULL;
static void *_kmTimer0CallbackUserDataOVF = NULL;

static kmTimer0CallbackType *_timerCallbackCompA = NULL;
static void *_kmTimer0CallbackUserDataCompA = NULL;

#ifndef OCR0
static kmTimer0CallbackType *_timerCallbackCompB = NULL;
static void *_kmTimer0CallbackUserDataCompB = NULL;
#endif

// "Private" functions

// "Public" functions
// Initializations
void kmTimer0Init(const uint8_t prescaler, const uint8_t modeA, const uint8_t modeB);
void kmTimer0InitOnPrescalerBottomToTopOvfCompABInterruptCallback(const uint8_t prescaler);
void kmTimer0InitOnAccuratePeriodGenerateOutputClockA(const uint32_t period);
void kmTimer0InitOnAccurateTimeCompAInterruptCallback(const uint32_t microseconds, const bool outOnOC0A);
void kmTimer0InitExternal(bool falling);

#ifndef OCR0
void kmTimer0InitOnAccurateTimeCompABInterruptCallback(const uint32_t microseconds, uint8_t phaseIntB);
uint8_t kmTimer0InitOnAccurateTimeFastPwm(const uint32_t microseconds, const bool outputCompA, const uint8_t dutyB, const bool invertedB);
uint8_t kmTimer0InitOnAccurateTimePcPwm(const uint32_t microseconds, const bool outputCompA, const uint8_t dutyB, const bool invertedB);
void kmTimer0InitOnPrescalerBottomToTopFastPwm(const uint8_t prescaler, const uint8_t dutyA, const bool invertedA, const uint8_t dutyB, const bool invertedB);
void kmTimer0InitOnPrescalerBottomToTopPcPwm(const uint8_t prescaler, const uint8_t dutyA, const bool invertedA, const uint8_t dutyB, const bool invertedB);
#else
void kmTimer0InitOnPrescalerBottomToTopFastPwm(const uint8_t prescaler, const uint8_t dutyA, const bool invertedA);
void kmTimer0InitOnPrescalerBottomToTopPcPwm(const uint8_t prescaler, const uint8_t dutyA, const bool invertedA);
#endif

// Setters callback user data
void kmTimer0SetCallbackUserDataOVF(void *userData);
void kmTimer0SetCallbackUserDataCompA(void *userData);
#ifndef OCR0
void kmTimer0SetCallbackUserDataCompB(void *userData);
#endif

// Registering callbacks
void kmTimer0RegisterCallbackOVF(void *userData, void (*callback)(void *));
void kmTimer0RegisterCallbackCompA(void *userData, void (*callback)(void *));
#ifndef OCR0
void kmTimer0RegisterCallbackCompB(void *userData, void (*callback)(void *));
#endif

// Unregistering callbacks
void kmTimer0UnregisterCallbackOVF(void);
void kmTimer0UnregisterCallbackCompA(void);
#ifndef OCR0
void kmTimer0UnregisterCallbackCompB(void);
#endif

// Enabling interrupts
void kmTimer0EnableInterruptOVF(void);
void kmTimer0EnableInterruptCompA(void);
#ifndef OCR0
void kmTimer0EnableInterruptCompB(void);
#endif

// Disabling interrupts
void kmTimer0DisableInterruptsAll(void);
void kmTimer0DisableInterruptOVF(void);
void kmTimer0DisableInterruptCompA(void);
#ifndef OCR0
void kmTimer0DisableInterruptCompB(void);
#endif

// Setters for Comparator values
void kmTimer0SetValueCompA(uint8_t value);
#ifndef OCR0
void kmTimer0SetValueCompB(uint8_t value);
#endif

// Configuration of Comparator Outputs
void kmTimer0ConfigureOCA(uint8_t compareOutputMode);
#ifndef OCR0
void kmTimer0ConfigureOCB(uint8_t compareOutputMode);
#endif

// Controlling timer flow
void kmTimer0Start(void);
void kmTimer0Stop(void);
void kmTimer0Restart(void);

// Controlling PWM
void kmTimer0SetPwmDutyBottomToTop(Tcc0PwmOut pwmOut, uint8_t duty);
void kmTimer0SetPwmInversion(Tcc0PwmOut pwmOut, bool inverted);

// Timer flow calculations
uint8_t kmTimer0CalcPerdiod(const uint32_t microseconds, uint8_t *prescaler);
uint8_t kmTimer0CalcPerdiodWithMinPwmAccuracy(const uint32_t microseconds, uint8_t *prescaler, const uint8_t minimumCycleAcuracy);
uint8_t kmTimer0CalcDutyOnCycles(uint8_t duty, uint8_t cycles);

// Implementation
void kmTimer0Init(const uint8_t prescaler, const uint8_t modeA, const uint8_t modeB) {
	kmTimer0Stop();
	_kmTimer0PrescalerSelectBits = prescaler;
	TCCR0A = (TCCR0A & ~KM_TCC0_MODE_MASK_A) | modeA;
#ifndef OCR0
	TCCR0B = (TCCR0B & ~KM_TCC0_MODE_MASK_B) | modeB;
#endif
}

void kmTimer0InitOnPrescalerBottomToTopOvfCompABInterruptCallback(const uint8_t prescaler) {
	kmTimer0Init(prescaler, KM_TCC0_MODE_3_A, KM_TCC0_MODE_3_B);
}

void kmTimer0InitOnAccuratePeriodGenerateOutputClockA(const uint32_t periodInMicroseconds) {
	kmTimer0InitOnAccurateTimeCompAInterruptCallback(periodInMicroseconds >> KMC_DIV_BY_2, true);
}

void kmTimer0InitOnAccurateTimeCompAInterruptCallback(const uint32_t microseconds, const bool outOnOC0A) {
	_kmTimer0CompACycles = kmTimer0CalcPerdiod(microseconds, &_kmTimer0PrescalerSelectBits);
	kmTimer0Init(_kmTimer0PrescalerSelectBits, KM_TCC0_MODE_2_A, KM_TCC0_MODE_2_B);
	kmTimer0SetValueCompA(_kmTimer0CompACycles);
	kmTimer0ConfigureOCA(outOnOC0A ? KM_TCC0_A_PWM_COMP_OUT_TOGGLE : KM_TCC0_A_PWM_COMP_OUT_NORMAL);
}

#ifndef OCR0
void kmTimer0InitOnAccurateTimeCompABInterruptCallback(const uint32_t microseconds, uint8_t phaseIntB) {
	_kmTimer0CompACycles = kmTimer0CalcPerdiod(microseconds, &_kmTimer0PrescalerSelectBits);
	_kmTimer0CompBCycles = kmTimer0CalcDutyOnCycles(phaseIntB, _kmTimer0CompACycles);
	kmTimer0Init(_kmTimer0PrescalerSelectBits, KM_TCC0_MODE_2_A, KM_TCC0_MODE_2_B);
	kmTimer0SetValueCompA(_kmTimer0CompACycles);
	kmTimer0SetValueCompB(_kmTimer0CompBCycles);
}
#endif

void kmTimer0InitOnPrescalerBottomToTopFastPwmOCA(const uint8_t prescaler
												, const uint8_t dutyA
												, const bool invertedA) {
	kmTimer0Init(prescaler, KM_TCC0_MODE_3_A, KM_TCC0_MODE_3_B);
	kmTimer0SetValueCompA(KM_TCC0_TOP);

	if (dutyA > 0u) {
		kmTimer0ConfigureOCA(invertedA ? KM_TCC0_A_PWM_COMP_OUT_SET_UP_CLEAR_DOWN : KM_TCC0_A_PWM_COMP_OUT_CLEAR_UP_SET_DOWN);
		kmTimer0SetValueCompA(dutyA);
	}
}

#ifndef OCR0
void kmTimer0InitOnPrescalerBottomToTopFastPwm(const uint8_t prescaler
											, const uint8_t dutyA
											, const bool invertedA
											, const uint8_t dutyB
											, const bool invertedB) {
	kmTimer0InitOnPrescalerBottomToTopFastPwmOCA(prescaler, dutyA, invertedA);
	if (dutyB > KMC_UNSIGNED_ZERO) {
		kmTimer0ConfigureOCB(invertedB ? KM_TCC0_B_PWM_COMP_OUT_SET_UP_CLEAR_DOWN : KM_TCC0_B_PWM_COMP_OUT_CLEAR_UP_SET_DOWN);
		kmTimer0SetValueCompB(dutyB);
	}
}
#endif

#ifndef OCR0
uint8_t kmTimer0InitOnAccurateTimeFastPwm(const uint32_t microseconds
											, const bool outputCompA
											, const uint8_t dutyB
											, const bool invertedB
											) {
	
	_kmTimer0CompACycles = kmTimer0CalcPerdiodWithMinPwmAccuracy(microseconds, &_kmTimer0PrescalerSelectBits, KM_TCC0_MINIMUM_PWM_CYCCLE_ACCURACY);
	kmTimer0Init(_kmTimer0PrescalerSelectBits, KM_TCC0_MODE_7_A, KM_TCC0_MODE_7_B);
	kmTimer0SetValueCompA(_kmTimer0CompACycles);

	if (true == outputCompA) {
		kmTimer0ConfigureOCA(KM_TCC0_A_PWM_COMP_OUT_TOGGLE);
	}

	if (dutyB > KMC_UNSIGNED_ZERO) {
		_kmTimer0CompBCycles = kmTimer0CalcDutyOnCycles(dutyB, _kmTimer0CompACycles);
		kmTimer0SetValueCompB(_kmTimer0CompBCycles);
		kmTimer0ConfigureOCB(invertedB ? KM_TCC0_B_PWM_COMP_OUT_SET_UP_CLEAR_DOWN : KM_TCC0_B_PWM_COMP_OUT_CLEAR_UP_SET_DOWN);
	}
	return _kmTimer0CompACycles;
}
#endif

void kmTimer0InitOnPrescalerBottomToTopPcPwmOCA(const uint8_t prescaler
												, const uint8_t dutyA
												, const bool invertedA) {
	kmTimer0Init(prescaler, KM_TCC0_MODE_1_A, KM_TCC0_MODE_1_B);

	kmTimer0SetValueCompA(dutyA);
	kmTimer0ConfigureOCA(invertedA ? KM_TCC0_A_PWM_COMP_OUT_SET_UP_CLEAR_DOWN : KM_TCC0_A_PWM_COMP_OUT_CLEAR_UP_SET_DOWN);
}

#ifndef OCR0
void kmTimer0InitOnPrescalerBottomToTopPcPwm(const uint8_t prescaler
											, const uint8_t dutyA
											, const bool invertedA
											, const uint8_t dutyB
											, const bool invertedB) {
	kmTimer0InitOnPrescalerBottomToTopPcPwmOCA(prescaler, dutyA, invertedA);
	kmTimer0SetValueCompB(dutyB);
	kmTimer0ConfigureOCB(invertedB ? KM_TCC0_B_PWM_COMP_OUT_SET_UP_CLEAR_DOWN : KM_TCC0_B_PWM_COMP_OUT_CLEAR_UP_SET_DOWN);
}
#endif

#ifndef OCR0
uint8_t kmTimer0InitOnAccurateTimePcPwm(const uint32_t microseconds
										, const bool outputCompA
										, const uint8_t dutyB
										, const bool invertedB
										) {
	_kmTimer0CompACycles = kmTimer0CalcPerdiodWithMinPwmAccuracy(microseconds >> KMC_DIV_BY_2, &_kmTimer0PrescalerSelectBits, KM_TCC0_MINIMUM_PWM_CYCCLE_ACCURACY);
	kmTimer0SetValueCompA(_kmTimer0CompACycles);
	kmTimer0Init(_kmTimer0PrescalerSelectBits, KM_TCC0_MODE_5_A, KM_TCC0_MODE_5_B);
	
	if (true == outputCompA) {
		kmTimer0ConfigureOCA(KM_TCC0_A_PWM_COMP_OUT_TOGGLE);
	}

	if (dutyB > KMC_UNSIGNED_ZERO) {
		_kmTimer0CompBCycles = kmTimer0CalcDutyOnCycles(dutyB, _kmTimer0CompACycles);
		kmTimer0SetValueCompB(_kmTimer0CompBCycles);
		kmTimer0ConfigureOCB(invertedB ? KM_TCC0_B_PWM_COMP_OUT_SET_UP_CLEAR_DOWN : KM_TCC0_B_PWM_COMP_OUT_CLEAR_UP_SET_DOWN);
	}
	return _kmTimer0CompACycles;
}
#endif

void kmTimer0InitExternal(bool falling) {
	if (true == falling) {
		_kmTimer0PrescalerSelectBits = KM_TCC0_EXT_T0_FAL;
		} else {
		_kmTimer0PrescalerSelectBits = KM_TCC0_EXT_T0_RIS;
	}
}

uint8_t kmTimer0CalcDutyOnCycles(uint8_t duty, uint8_t cyclesRange) {
	uint16_t result = ((uint16_t)(cyclesRange * duty)) / KM_TCC0_TOP;
	return (uint8_t)result;
}

void kmTimer0SetPwmDutyAccurateTimeModes(uint8_t duty) {
#ifndef OCR0
	kmTimer0SetValueCompB(kmTimer0CalcDutyOnCycles(duty, _kmTimer0CompACycles));
#endif
}

void kmTimer0SetPwmDutyBottomToTop(Tcc0PwmOut pwmOut, uint8_t duty) {
	switch (pwmOut) {
		case KM_TCC0_PWM_OUT_A: {
			kmTimer0SetValueCompA(duty);
			break;
		}
#ifndef OCR0
		case KM_TCC0_PWM_OUT_B: {
			kmTimer0SetValueCompB(duty);
			break;
		}
#endif
		// no default
	}
}

uint8_t kmTimer0CalcPerdiod(const uint32_t microseconds, uint8_t *prescaler) {
	uint64_t cycles = (int64_t)(F_CPU);
	cycles *= microseconds;
	cycles /= KMC_CONV_MICROSECONDS_TO_SECONCS;
	if (cycles <= KM_TCC0_TOP) {
		// no prescaler, full XTAL
		*prescaler = KM_TCC0_PRSC_1;
	} else if ((cycles >>= KMC_DIV_BY_8) <= KM_TCC0_TOP) {
		// prescaler by /8
		*prescaler = KM_TCC0_PRSC_8;
	} else if ((cycles >>= KMC_DIV_BY_8) <= KM_TCC0_TOP) {
		// prescaler by /64
		*prescaler = KM_TCC0_PRSC_64;
	} else if ((cycles >>= KMC_DIV_BY_4) <= KM_TCC0_TOP) {
		// prescaler by /256
		*prescaler = KM_TCC0_PRSC_256;
	} else if ((cycles >>= KMC_DIV_BY_4) <= KM_TCC0_TOP) {
		// prescaler by /1024
		*prescaler = KM_TCC0_PRSC_1024;
	} else {
		// request was out of bounds, set as maximum
		*prescaler = KM_TCC0_PRSC_1024;
		cycles = KM_TCC0_TOP;
	}
	return (uint8_t)(cycles != KMC_UNSIGNED_ZERO ? cycles - KM_TCC0_CYCLES_CALCULATION_CORRECTION : cycles);
}

uint8_t kmTimer0CalcPerdiodWithMinPwmAccuracy(const uint32_t microseconds, uint8_t *prescaler, const uint8_t minimumCycleAcuracy) {
	uint8_t result = kmTimer0CalcPerdiod(microseconds, prescaler);
	
	if (result < minimumCycleAcuracy) {
		result = minimumCycleAcuracy;
	}
	
	return result;
}

void kmTimer0SetPwmInversion(Tcc0PwmOut pwmOut, bool inverted) {
	switch (pwmOut) {
		case KM_TCC0_PWM_OUT_A: {
			if (false == inverted) {
				kmTimer0ConfigureOCA(KM_TCC0_A_PWM_COMP_OUT_CLEAR_UP_SET_DOWN);
			} else {
				kmTimer0ConfigureOCA(KM_TCC0_A_PWM_COMP_OUT_SET_UP_CLEAR_DOWN);
			}
			break;
		}
#ifndef OCR0
		case KM_TCC0_PWM_OUT_B: {
			if (false == inverted) {
				kmTimer0ConfigureOCB(KM_TCC0_B_PWM_COMP_OUT_CLEAR_UP_SET_DOWN);
			} else {
				kmTimer0ConfigureOCB(KM_TCC0_B_PWM_COMP_OUT_SET_UP_CLEAR_DOWN);
			}
			break;
		}
#endif
		default: {
			// intentionally
		}
	}
}

void kmTimer0Start(void) {
	TCCR0B = (TCCR0B & ~KM_TCC0_CS_MASK) | _kmTimer0PrescalerSelectBits;
}

void kmTimer0Stop(void) {
	TCCR0B &= (TCCR0B & ~KM_TCC0_CS_MASK) | KM_TCC0_STOP;
}

void kmTimer0Restart(void) {
	TCNT0 = KMC_UNSIGNED_ZERO;
}

void kmTimer0ConfigureOCA(uint8_t compareOutputMode) {
	// Do not touch pin configuration if NORMAL mode to be used and other mode never used
	if (KM_TCC0_A_PWM_COMP_OUT_NORMAL == compareOutputMode && false == _kmTimer0OtputComparePinUsedA) {
		return;
	}
	
	KM_TCC0_PWM_DDR |= KM_TCC0_PWM_BV_A;		// setup port for output
	KM_TCC0_PWM_PORT |= KM_TCC0_PWM_BV_A;		// HIGH by default
	TCCR0A = (TCCR0A & ~KM_TCC0_COMA_MASK) | compareOutputMode;
	_kmTimer0OtputComparePinUsedA = true;
}

void kmTimer0ConfigureOCB(uint8_t compareOutputMode) {
#ifndef OCR0
	// Do not touch pin configuration if NORMAL mode to be used and other mode never used
	if (KM_TCC0_B_PWM_COMP_OUT_NORMAL == compareOutputMode && false == _kmTimer0OtputComparePinUsedB) {
		return;
	}
#ifndef KM_TCC0_PWM_DDR_OCB
	KM_TCC0_PWM_DDR |= KM_TCC0_PWM_BV_B;		// setup port for output
	KM_TCC0_PWM_PORT |= KM_TCC0_PWM_BV_B;		// HIGH by default
#else
	KM_TCC0_PWM_DDR_OCB |= KM_TCC0_PWM_BV_B;	// setup port for output
	KM_TCC0_PWM_PORT_OCB |= KM_TCC0_PWM_BV_B;	// HIGH by default
#endif
	TCCR0A = (TCCR0A & ~KM_TCC0_COMB_MASK) | compareOutputMode;
	_kmTimer0OtputComparePinUsedB = true;
#endif
}

void kmTimer0SetValueCompA(uint8_t value) {
	OCR0A = _kmTimer0CompACycles = value; // -V2561
}

#ifndef OCR0
void kmTimer0SetValueCompB(uint8_t value) {
	OCR0B = _kmTimer0CompBCycles = value; // -V2561
}
#endif

void kmTime0SetPrescale(uint8_t prescaler) {
	_kmTimer0PrescalerSelectBits = prescaler;
	kmTimer0Start();
}

void kmTimer0EnableInterruptCompA(void) {
	TIMSK0 |= _BV(OCIE0A);
}

#ifndef OCR0
void kmTimer0EnableInterruptCompB(void) {
	TIMSK0 |= _BV(OCIE0B);
}
#endif

void kmTimer0EnableInterruptOVF(void) {
	TIMSK0 |= _BV(TOIE0);
}

void kmTimer0DisableInterruptCompA(void) {
	TIMSK0 &= ~_BV(OCIE0A);
}

#ifndef OCR0
void kmTimer0DisableInterruptCompB(void) {
	TIMSK0 &= ~_BV(OCIE0B);
}
#endif

void kmTimer0DisableInterruptOVF(void) {
	TIMSK0 &= ~_BV(TOIE0);
}

void kmTimer0DisableInterruptsAll(void) {
#ifndef OCR0
	TIMSK0 &= ~(_BV(OCIE0B) | _BV(OCIE0A) | _BV(TOIE0));
#else
	TIMSK0 &= ~(_BV(OCIE0A) | _BV(TOIE0));
#endif /* OCR0 */
}

void kmTimer0RegisterCallbackCompA(void *userData, kmTimer0CallbackType *callback) {
	_timerCallbackCompA = callback;
	_kmTimer0CallbackUserDataCompA = userData;
}

#ifndef OCR0
void kmTimer0RegisterCallbackCompB(void *userData, kmTimer0CallbackType *callback) {
	_timerCallbackCompB = callback;
	_kmTimer0CallbackUserDataCompB = userData;
}
#endif

void kmTimer0RegisterCallbackOVF(void *userData, kmTimer0CallbackType *callback) {
	_timerCallbackOVF = callback;
	_kmTimer0CallbackUserDataOVF = userData;
}

void kmTimer0UnregisterCallbackCompA(void) {
	_kmTimer0CallbackUserDataCompA = NULL;
}

#ifndef OCR0
void kmTimer0UnregisterCallbackCompB(void) {
	_kmTimer0CallbackUserDataCompB = NULL;
}
#endif /* OCR0 */

void kmTimer0UnregisterCallbackOVF(void) {
	_kmTimer0CallbackUserDataOVF = NULL;
}

void kmTimer0SetCallbackUserDataOVF(void *userData)  {
	ATOMIC_BLOCK(ATOMIC_RESTORESTATE) {
		_kmTimer0CallbackUserDataOVF = userData;
	}
}

void kmTimer0SetCallbackUserDataCompA(void *userData)  {
	ATOMIC_BLOCK(ATOMIC_RESTORESTATE) {
		_kmTimer0CallbackUserDataCompA = userData;
	}
}

#ifndef OCR0
void kmTimer0SetCallbackUserDataCompB(void *userData)  {
	ATOMIC_BLOCK(ATOMIC_RESTORESTATE) {
		_kmTimer0CallbackUserDataCompB = userData;
	}
}
#endif /* OCR0 */

ISR(TIMER0_COMPA_vect, ISR_NOBLOCK) {
	if (NULL != _timerCallbackCompA) {
		_timerCallbackCompA(_kmTimer0CallbackUserDataCompA);
	}
}

#ifndef OCR0
ISR(TIMER0_COMPB_vect, ISR_NOBLOCK) {
	if (NULL != _timerCallbackCompB) {
		_timerCallbackCompB(_kmTimer0CallbackUserDataCompB);
	}
}
#endif /* OCR0 */

ISR(TIMER0_OVF_vect, ISR_NOBLOCK) {
	if (NULL != _timerCallbackOVF) {
		_timerCallbackOVF(_kmTimer0CallbackUserDataOVF);
	}
}
